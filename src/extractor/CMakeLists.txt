set(TARGET ldm-extractor)

set(SOURCES
    lomiri/downloads/extractor/deflator.cpp
    lomiri/downloads/extractor/factory.cpp
    lomiri/downloads/extractor/main.cpp
    lomiri/downloads/extractor/unzip.cpp
)

set(HEADERS
    lomiri/downloads/extractor/deflator.h
    lomiri/downloads/extractor/factory.h
    lomiri/downloads/extractor/unzip.h
)

include_directories(${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(${CMAKE_SOURCE_DIR}/src/common/priv)

add_executable(${TARGET}
	${HEADERS}
	${SOURCES}
)

target_link_libraries(${TARGET}
	glog::glog
	Qt5::Core
	${Boost_LIBRARIES}
	ldm-priv-common
)

install(TARGETS ${TARGET} DESTINATION ${CMAKE_INSTALL_FULL_LIBEXECDIR}/lomiri-download-manager)
