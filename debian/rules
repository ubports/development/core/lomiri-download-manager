#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1
#export DPKG_GENSYMBOLS_CHECK_LEVEL=4

include /usr/share/dpkg/architecture.mk

# see FEATURE AREAS in dpkg-buildflags(1)
export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed

export QT_SELECT := 5

BUILDHOME=$(CURDIR)/debian/build

override_dh_clean:
	dh_clean
	rm -rfv $(BUILDHOME)

override_dh_auto_configure:
	dh_auto_configure -- -DCMAKE_INSTALL_LIBEXECDIR=/usr/lib/$(DEB_HOST_MULTIARCH) -DENABLE_UBUNTU_COMPAT=ON

override_dh_auto_test:
	mkdir -p $(BUILDHOME)
	HOME=$(BUILDHOME) dh_auto_test

override_dh_auto_install:
	dh_auto_install
	rm -v debian/tmp/usr/lib/*/libldm-priv-common.so

override_dh_install:
	# liblomiri-download-manager-client-doc
	mkdir -p "$(CURDIR)/debian/tmp/usr/share/doc/liblomiri-download-manager-client-doc/"
	mv $(CURDIR)/debian/tmp/usr/share/doc/lomiri-download-manager/cpp/* $(CURDIR)/debian/tmp/usr/share/doc/liblomiri-download-manager-client-doc/
	find "$(CURDIR)/debian/tmp/usr/share/doc/liblomiri-download-manager-client-doc/" -name *.md5 -delete
	rdfind -makesymlinks true "$(CURDIR)/debian/tmp/usr/share/doc/liblomiri-download-manager-client-doc/"
	symlinks -rc "$(CURDIR)/debian/tmp/usr/share/doc/liblomiri-download-manager-client-doc/"
	# qml-module-lomiri-downloadmanager
	mkdir -p "$(CURDIR)/debian/tmp/usr/share/doc/qml-module-lomiri-downloadmanager-doc/"
	mv $(CURDIR)/debian/tmp/usr/share/doc/lomiri-download-manager/qml/* $(CURDIR)/debian/tmp/usr/share/doc/qml-module-lomiri-downloadmanager-doc/
	find "$(CURDIR)/debian/tmp/usr/share/doc/qml-module-lomiri-downloadmanager-doc/" -name *.md5 -delete
	rdfind -makesymlinks true "$(CURDIR)/debian/tmp/usr/share/doc/qml-module-lomiri-downloadmanager-doc/"
	symlinks -rc "$(CURDIR)/debian/tmp/usr/share/doc/qml-module-lomiri-downloadmanager-doc/"
	# this directory is empty...
	rmdir debian/tmp/usr/share/doc/qml-module-lomiri-downloadmanager-doc/html/images/
	dh_install

override_dh_missing:
	dh_missing --fail-missing

# Add migrations add-on when build on Ubuntu & original name is available.
DH_MIGRATIONS = $(shell type dh_migrations >/dev/null 2>&1 && echo "--with migrations")

%:
	dh $@ $(DH_MIGRATIONS)

override_dh_installdeb:
	dh_apparmor --profile-name=usr.lib.lomiri-download-manager.ldm-extractor -plomiri-download-manager
	dh_installdeb

override_dh_installchangelogs:
	dh_installchangelogs NEWS

get-orig-source:
	uscan --noconf --force-download --rename --download-current-version --destdir=..
